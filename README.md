# README #

This app was created for a project for iOS App Development. There are parts of it that are not complete, however the overall idea is that users can register for an account, add books that they have, and search for books that other users may have.

When the user wants to add a book, there is a search through the Google Books API to select the book from Google Books. After selecting a book, user can set a price and then save the book. When other users search for that book using the Find a Book feature, they can see the book, the users name, and the price he set for it. Then there is a button that allows them to message that user to negotiate a deal for the book.
