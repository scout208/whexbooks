//
//  step2ViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class step2ViewController: UIViewController {

    var bookBeingAdded: Book!
    
    @IBOutlet weak var dollarAmountLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(bookBeingAdded.title)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "step2to4" {
            bookBeingAdded.price = dollarAmountLabel.text
            
            if dollarAmountLabel.text == nil{
                bookBeingAdded.price = "0"
            }
            if let destinationVC = segue.destinationViewController as? step4ViewController {
                destinationVC.bookBeingAdded = bookBeingAdded
                bookBeingAdded.price = dollarAmountLabel.text
            }
        }
    }
    
    @IBAction func stepperActionListener(sender: UIStepper)
    {
        dollarAmountLabel.text = Int(sender.value).description
    }
}
