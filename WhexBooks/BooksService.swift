//
//  BooksService.swift
//  Google Books Tester
//
//  Created by uics14 on 11/25/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import Foundation

extension String {
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let characterSet = NSMutableCharacterSet.alphanumericCharacterSet()
        characterSet.addCharactersInString("-._* ")
        
        return stringByAddingPercentEncodingWithAllowedCharacters(characterSet)?.stringByReplacingOccurrencesOfString(" ", withString: "+")
    }
}

struct BooksService {
    
    let booksAPIKey: String
    let booksBaseURL: NSURL?
    
    
    init(APIKey: String) {
        booksAPIKey = APIKey
        booksBaseURL = NSURL(string: "https://www.googleapis.com/books/v1/")!
    }
    
    func getBooks(query: String, completion: ([Book]? -> Void)) {
        let escapedQuery = query.stringByAddingPercentEncodingForURLQueryValue()
        if let booksURL = NSURL(string: "https://www.googleapis.com/books/v1/volumes?q=\(escapedQuery!)&key=\(booksAPIKey)") {
            let networkOperation = NetworkOperation(url: booksURL)
            
            networkOperation.downloadJSONFromURL {
                (let JSONDictionary) in
                let books = self.booksFromJSON(JSONDictionary)
                completion(books)
            }
            
        } else {
            print("Could not construct a valid URL")
        }
    }
    
    func booksFromJSON(jsonDictionary: JSON?) -> [Book]? {
        if let booksArray = jsonDictionary!["items"].array {
            var books = [Book]()
            
            for bookDict in booksArray {
                books.append(Book(bookDictionary: bookDict["volumeInfo"]))
            }
                                
            return books
        } else {
            print("JSON dictionary returned nil for 'items' key")
            return nil
        }
    }
    
}