//
//  MessagingViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 12/3/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class MessagingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var TableView: UITableView!
    var messages = [Message]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "messageCell", bundle: nil)
        TableView.registerNib(nib, forCellReuseIdentifier: "messageCell")
        
        loadFromMessages()
        loadToMessages()
        
    }
    
    func loadFromMessages()
    {
        
        let query = PFQuery(className:"message")
        let curUser = PFUser.currentUser()
        
        
        query.whereKey("toUser", equalTo: curUser!)
        query.findObjectsInBackgroundWithBlock{
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        
                        var message = Message()
                        let lastActive = object.valueForKey("createdAt")
                        
                        if lastActive != nil {
                            let dateFormatter = NSDateFormatter()
                            //dateFormatter.dateFormat = "yyyy-'MM'-'dd 'at' HH:mm"
                            dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
                            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                            dateFormatter.timeZone = NSTimeZone.localTimeZone()
                            let date = dateFormatter.stringFromDate(lastActive as! NSDate)
                            message.date = date
                        }
                        
                        //message.fromUserLabel = object.valueForKey("fromUserUsername") as! String
                        message.fromUser = object.valueForKey("fromUser") as! PFUser
                        message.fromUserLabel = object.valueForKey("fromUserUsername") as! String
                        //message.setFromUser(object.valueForKey("fromUser") as! PFUser)
                        //message.setFromUser(nameId)
                        message.bookName = object.valueForKey("bookName") as! String
                        message.message = object.valueForKey("messageContent") as! String
                        message.messageIncoming = true
                        
                        self.messages.append(message)
                        self.TableView.reloadData()
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    func loadToMessages()
    {
        
        let query = PFQuery(className:"message")
        let curUser = PFUser.currentUser()
        
        query.whereKey("fromUser", equalTo: curUser!)
        //query.includeKey("fromUser")
        query.findObjectsInBackgroundWithBlock{
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                //print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        
                        var message = Message()
                        let lastActive = object.valueForKey("createdAt")
                        
                        if lastActive != nil {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
                            dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                            dateFormatter.timeZone = NSTimeZone.localTimeZone()
                            let date = dateFormatter.stringFromDate(lastActive as! NSDate)
                            message.date = date
                        }
                        //message.setFromUser(PFUser.currentUser()!)
                        //let nameId = object.objectForKey("toUser") as! PFUser
                        //print(nameId)
                        //let username = nameId.username! as String
                        //print(username)
                        
                        
                        //message.cellString = username
                        message.fromUserLabel = object.valueForKey("toUserUsername") as! String
                        message.bookName = object.valueForKey("bookName") as! String
                        message.message = object.valueForKey("messageContent") as! String
                        message.messageIncoming = false
                        
                        
                        self.messages.append(message)
                        self.TableView.reloadData()
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let Cell: messageCellClass = TableView.dequeueReusableCellWithIdentifier("messageCell") as! messageCellClass
        let message = messages[indexPath.row]
        Cell.loadItem(message)
        return Cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var messageSelected = Message()
        var messageToSend = Message()
        
        
        print("did select row \(indexPath.row)")
        
        let rowSelected:Int = indexPath.row
        messageSelected = messages[rowSelected]
        
        print(messageSelected)
        
        var tField: UITextField!
        var text: String = "";
        
        func configurationTextField(textField: UITextField!){
            textField.placeholder = "Enter a Message"
            tField = textField
        }
        
        
        func handleCancel(alertView: UIAlertAction!){
            print("Cancelled !!")
        }
        
        let input:String = messageSelected.message
        
        
        if messageSelected.messageIncoming == true{
            let alert = UIAlertController(title: "Message Content:\n" + input + "\n\n Your Response:" , message: "", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addTextFieldWithConfigurationHandler(configurationTextField)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
                text = tField.text!
                print(text)
                
                //messageToSend.setFromUser(messageSelected.toUser)
                messageToSend.fromUser = PFUser.currentUser()!
                messageToSend.fromUserLabel = PFUser.currentUser()?.username
                messageToSend.toUserLabel = messageSelected.fromUserLabel
                messageToSend.toUser = messageSelected.fromUser
                messageToSend.bookName = messageSelected.bookName
                messageToSend.message = text
                self.messages.append(messageToSend)
                
                let newMessage = PFObject(className: "message")
                newMessage["toUser"] = messageSelected.fromUser
                newMessage["fromUserUsername"] = PFUser.currentUser()?.username
                newMessage["toUserUsername"] = messageSelected.fromUserLabel
                newMessage["fromUser"] = PFUser.currentUser()
                newMessage["bookName"] = messageSelected.bookName
                newMessage["status"] = "incoming"
                newMessage["createdBy"] = PFUser.currentUser()
                newMessage["messageContent"] = text
                do {
                    try newMessage.save()
                    print("Did it")
                }catch{
                    print("Error")
                }
                
                self.TableView.reloadData()
                
            }))
            self.presentViewController(alert, animated: true, completion: {
                
            })
        }
        else {
            let alert = UIAlertController(title: "You said " + input, message: "", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:handleCancel))
            
            self.presentViewController(alert, animated: true, completion: {
                
            })
        }
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            messages.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
}