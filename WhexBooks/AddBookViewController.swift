//
//  addBookViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit
import AVFoundation

class AddBookViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
    let session : AVCaptureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer!
    var highlightView : UIView = UIView()
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var autocompleteTableView: UITableView!
    
    var autocompleteBooks = [Book]()
    
    private let booksAPIKey = "AIzaSyCaeWV0hkGyxfXVEMHdijhaHOIIA0im1UQ"
    
    var bookBeingAdded = Book()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        autocompleteTableView.scrollEnabled = true
        
        let nib = UINib(nibName: "BookAutocompleteTableViewCell", bundle: nil)
        
        autocompleteTableView.registerNib(nib, forCellReuseIdentifier: "BookAutocompleteTableViewCell")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToNextStep(sender: AnyObject) {
        performSegueWithIdentifier("step1to2", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "step1to2"
        {
            if let destinationVC = segue.destinationViewController as? step2ViewController{
                destinationVC.bookBeingAdded = bookBeingAdded
            }
        }
    }
    
    @IBAction func scanBarCode(sender: AnyObject) {

        self.highlightView.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleLeftMargin, .FlexibleRightMargin]
        
        // Select the color you want for the completed scan reticle
        self.highlightView.layer.borderColor = UIColor.greenColor().CGColor
        self.highlightView.layer.borderWidth = 3
        
        // Add it to our controller's view as a subview.
        self.view.addSubview(self.highlightView)
        
        
        // For the sake of discussion this is the camera
        let Capturedevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        // Create a nilable NSError to hand off to the next method.
        // Make sure to use the "var" keyword and not "let"
        let error : NSError? = nil
        
        let input: AVCaptureDeviceInput? = nil
        
        
        do {
            let input: AVCaptureDeviceInput? = try AVCaptureDeviceInput(device: Capturedevice) as AVCaptureDeviceInput
        } catch{
            print("error")
        }
        
        
        // If our input is not nil then add it to the session, otherwise we're kind of done!
        
        if input != nil {
            session.addInput(input)
        }
        else {
            // This is fine for a demo, do something real with this in your app. :)
            print(error)
        }
        
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        session.addOutput(output)
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        
        
        previewLayer = AVCaptureVideoPreviewLayer(layer: session) as AVCaptureVideoPreviewLayer
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.view.layer.addSublayer(previewLayer)
        
        // Start the scanner. You'll have to end it yourself later.
        session.startRunning()
    }

    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        var highlightViewRect = CGRectZero
        
        var barCodeObject : AVMetadataObject!
        
        var detectionString : String!
        
        let barCodeTypes = [AVMetadataObjectTypeUPCECode,
            AVMetadataObjectTypeCode39Code,
            AVMetadataObjectTypeCode39Mod43Code,
            AVMetadataObjectTypeEAN13Code,
            AVMetadataObjectTypeEAN8Code,
            AVMetadataObjectTypeCode93Code,
            AVMetadataObjectTypeCode128Code,
            AVMetadataObjectTypePDF417Code,
            AVMetadataObjectTypeQRCode,
            AVMetadataObjectTypeAztecCode
        ]
        
        
        // The scanner is capable of capturing multiple 2-dimensional barcodes in one scan.
        for metadata in metadataObjects {
            
            for barcodeType in barCodeTypes {
                
                if metadata.type == barcodeType {
                    barCodeObject = self.previewLayer.transformedMetadataObjectForMetadataObject(metadata as! AVMetadataMachineReadableCodeObject)
                    
                    highlightViewRect = barCodeObject.bounds
                    
                    detectionString = (metadata as! AVMetadataMachineReadableCodeObject).stringValue
                    
                    self.session.stopRunning()
                    break
                }
                
            }
        }
        
        print(detectionString)
        self.highlightView.frame = highlightViewRect
        self.view.bringSubviewToFront(self.highlightView)
        
    }
    
    
    // MARK: UITextFieldDelegate Functions
    
    func textFieldDidBeginEditing(textField: UITextField) {
        autocompleteTableView.hidden = false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let substring = (searchField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        searchBooksWithSubstring(substring)
        return true
    }
    
    func searchBooksWithSubstring(substring: String) {
        autocompleteBooks.removeAll(keepCapacity: false)
        
        let booksService = BooksService(APIKey: booksAPIKey)
        booksService.getBooks(substring) {
            (let bookResults) in
            self.autocompleteBooks = bookResults!
            dispatch_async(dispatch_get_main_queue()) {
                self.autocompleteTableView.reloadData()
            }
        }
        
        autocompleteTableView.hidden = false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true
    }
    
    
    // MARK: UITableViewDelegate Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteBooks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "BookAutocompleteTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! BookAutocompleteTableViewCell
        
        let book = autocompleteBooks[indexPath.row]
        
        cell.loadItem(book)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! BookAutocompleteTableViewCell
        searchField.text = selectedCell.bookTitleEdition.text
        
        bookBeingAdded.title = selectedCell.bookTitleEdition.text
        bookBeingAdded.authors = selectedCell.bookAuthor.text
        bookBeingAdded.image = selectedCell.bookImage.image
        bookBeingAdded.isbn_10 = selectedCell.bookISBN.text
        bookBeingAdded.isbn_13 = selectedCell.bookISBN13.text
        
        autocompleteTableView.hidden = true
        
        view.endEditing(true)
    }

}
