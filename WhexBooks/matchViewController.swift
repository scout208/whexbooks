//
//  matchViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/10/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit
import Foundation

class matchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MessageCellDelegate {
    
    // MARK: Outlets
    
    @IBOutlet var TableView: UITableView!
    
    
    // MARK: Variables and Constants
    
    var bookMatches = [Book]()
    var bookTitlesNeeded = [String]()
    var timer = NSTimer()
    var matchFound:Bool = false
    let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
    let globals:GlobalData = GlobalData.sharedInstance
    
    
    // MARK: UIViewController Functions
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "BookTableViewCell", bundle: nil)
        TableView.registerNib(nib, forCellReuseIdentifier: "BookTableViewCell")
        
        
        //dispatch_async(dispatch_get_global_queue(priority,0)){

        checkForBooksNeeded()
        //}
        //checkForMatches()
        
        
        /*while(!matchFound){
            self.checkForMatches()
            print(self.bookTitlesNeeded)
        }*/


    }
    
    override func viewDidAppear(animated: Bool) {
        TableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: Table View Delegate Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookMatches.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.TableView.dequeueReusableCellWithIdentifier("BookTableViewCell", forIndexPath: indexPath) as! BookTableViewCell
        
        let book = bookMatches[indexPath.row]
        
        //cell.messageButton.tag = indexPath.row
        
        //cell.messageButton.addTarget(self, action: "messageAction", forControlEvents: .TouchUpInside)
        
        cell.loadItem(book)
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.grayColor().CGColor
        
        cell.messageButton.tag = indexPath.row
        
        if cell.messageDelegate == nil {
            cell.messageDelegate = self
        }
        
        return cell
        
    }
    
    
    
    func checkForBooksNeeded()
    {
        let query = PFQuery(className:"book")
        let curUser = PFUser.currentUser()

        query.findObjectsInBackgroundWithBlock
        {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil{
                for object in objects!{
                    if let data = object as? PFObject{
                        
                        var book = Book()
                        book.title = data.valueForKey("bookName") as! String
                        book.person = data.valueForKey("neededUser") as? PFUser
                        
                        if self.globals.booksNeeded.contains(book.title) && curUser != book.person{
                            
                            print(book.title)
                            print("exists")
                            
                            book.person = data.valueForKey("neededUser") as? PFUser
                            book.personThatCreatedBook = data.valueForKey("CreatedBy") as? PFUser
                            book.userUsername = data.valueForKey("CreatedByUsername") as? String
                            book.isbn_10 = data.valueForKey("bookISBN") as? String
                            book.price = object.valueForKey("bookPrice") as? String
                            book.authors = object.valueForKey("bookAuthor") as? String
                            
                            let image = object.objectForKey("bookImage") as! PFFile
                            
                            image.getDataInBackgroundWithBlock{
                                (imageData: NSData?, error:NSError?) -> Void in
                                if error == nil {
                                    print("here")
                                    
                                    if let finalimage = UIImage(data: imageData!){
                                        book.image = finalimage
                                        self.bookMatches.append(book)
                                        self.TableView.reloadData()
                                    }
                                    //finalimage = UIImage(data:imageData!)!
                                    //book.image? = finalimage
                                    
                                }
                            }

                        }
                        else{

                        }
                        //Set test to total (assuming self.test is Int)
                        //self.test = data["total"] as! Int
                    }
                }
            }else{
                //Handle error
            }
        }
    }
    
    /*func checkForBooksNeeded()
    {
        let query = PFQuery(className:"book")
        let curUser = PFUser.currentUser()
        var bookNeededName: String = ""

        //print(globals.booksNeeded)
        //print(globals.booksNeeded[0])
        //print(curUser)
        
        for book in self.globals.booksNeeded {
            
            print("beginning new book, main thread paused")
            print(book)
            print("here")
        
                print("dispatched")
                query.whereKey("bookName", equalTo: book)
                query.findObjectsInBackgroundWithBlock{
                    (objects: [PFObject]?, error: NSError?) -> Void in
                    
                    
                    print("starting new Query")
                    if error == nil {
                        // The find succeeded.
                        print("Successfully retrieved \(objects!.count) scores.")
                        // Do something with the found objects
                        if let objects = objects {
                            for object in objects {
                                //print(object.objectId)
                                
                                var book = Book()
                                //print("here")
                                //book.image = object.valueForKey("bookImage") as! UIImage
                                book.authors = object.valueForKey("bookAuthor") as! String
                                book.isbn_10 = object.valueForKey("bookISBN") as! String
                                book.title = object.valueForKey("bookName") as! String
                                book.price = object.valueForKey("bookPrice") as? String
                                print(book.title)
                                book.person = object.valueForKey("CreatedBy") as? PFUser
                                bookNeededName = book.title
                                
                                if(book.person != nil){
                                    print("here")
                                    print(book.person!)
                                    book.person = object.valueForKey("CreatedBy") as! PFUser!
                                    book.userUsername = object.valueForKey("CreatedByUsername") as! String 
                                    //print(book.person)
                                    //print(book.person.username)
                                }
                                
                                let neededUser = object.valueForKey("neededUser") as? PFUser
                                if(neededUser == curUser)
                                {
                                    
                                }
                                else{
                                    //self.bookTitlesNeeded.append(bookNeededName)
                                    self.bookMatches.append(book)
                                    print("Match!!!")
                                    //self.TableView.reloadData()
                                }
                                //print(self.bookTitlesNeeded)
                                //print(book.person)
                                
                            }
                      
                        }
                        dispatch_async(dispatch_get_main_queue()){
                           // query.cancel()
                            //self.checkForMatches()
                            print("pick up main thread")
                            self.TableView.reloadData()
                        }
                    }
                    else {
                        // Log details of the failure
                        print("Error: \(error!) \(error!.userInfo)")
                    }
                }
        }
            //checkForMatches()
    }*/
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        //return 70
//    }
    
    
    
    @IBAction func messageAction(sender: UIButton)
    {
        print("message")
    }
    
    
    // MARK: ButtonCellDelegate
    
    func cellTapped(cell: BookTableViewCell) {
        //self.sendMessage("Message me!")
        
        
        //var messageSelected = Message()
        //var messageToSend = Message()
        
        //let cell = TableView.dequeueReusableCellWithIdentifier("BookBeingSoldCell") as! BookTableViewCell
        let book = bookMatches[cell.messageButton.tag]
        print(cell.messageButton.tag)
        print(book)
        var tField: UITextField!
        var text: String = "";
        
        
        
        func configurationTextField(textField: UITextField!){
            textField.placeholder = "Enter a Message"
            tField = textField
        }
        
        
        func handleCancel(alertView: UIAlertAction!){
            print("Cancelled !!")
        }
        
        
        let alert = UIAlertController(title: "Send Your Message To : \n" + book.userUsername, message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            text = tField.text!
            //print(text)
            
            // messageToSend.setFromUser(messageSelected.toUser)
            // messageToSend.setToUser(messageSelected.fromUser)
            // messageToSend.bookName = messageSelected.bookName
            //messageToSend.message = text
            //self.messages.append(messageToSend)
            
            print(book.personThatCreatedBook)
            print(book.userUsername)
            print(book.title)
            print(book.price)
            
            let newMessage = PFObject(className: "message")
            newMessage["toUser"] = book.personThatCreatedBook
            newMessage["toUserUsername"] = book.userUsername
            newMessage["fromUser"] = PFUser.currentUser()!
            newMessage["fromUserUsername"] = PFUser.currentUser()?.username
            newMessage["bookName"] = book.title
            newMessage["status"] = "incoming"
            newMessage["createdBy"] = PFUser.currentUser()
            newMessage["messageContent"] = text
            do {
                try newMessage.save()
                
                print("Did it")
            }catch{
                print("Error")
            }
            
            //self.TableView.reloadData()
            
        }))
        self.presentViewController(alert, animated: true, completion: {
            
        })
        
        
        
        
    }

}
