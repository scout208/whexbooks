//
//  ViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/3/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true;

        let currentUser = PFUser.currentUser()
        if currentUser != nil{
            performSegueWithIdentifier("toHomePage", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToLoginPage(sender: AnyObject) {
        performSegueWithIdentifier("login", sender: self)
    }
    
    @IBAction func GoToNextSegue(sender: AnyObject) {
        let username = self.usernameTextField.text
        let password = self.passwordTextField.text
        let email = self.emailTextField.text
        let finalEmail = email!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        // Validate the text fields
        if username?.characters.count < 5 {
           // var alert = UIAlertView(title: "Invalid", message: "Username must be greater than 5 characters", delegate: self, cancelButtonTitle: "OK")
            let alert = UIAlertController(title: "Invalid", message:"Username must be greater than 5 characters", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
            
            
        } else if password?.characters.count < 8 {
            let alert = UIAlertController(title: "Invalid", message:"Password must be greater than 8 characters", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
            
        } else if email?.characters.count < 8 {
            let alert = UIAlertController(title: "Invalid", message:"Please enter a valid email", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
            
        } else {
            // Run a spinner to show a task in progress
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            
            let newUser = PFUser()
            
             newUser.username = username
             newUser.password = password
             newUser.email = finalEmail
            
            //let newUser = PFObject(className: "_User")
            //newUser["rating"] = "5"
            //newUser["username"] = username
            //newUser["password"] = password
            //newUser["email"] = finalEmail
            //newUser.rating = "5"
            
            // Sign up the user asynchronously
            newUser.signUpInBackgroundWithBlock({ (succeed, error) -> Void in
                
                // Stop the spinner
                spinner.stopAnimating()
                if ((error) != nil) {
                    let alert = UIAlertController(title: "Invalid", message:"Error", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                    
                } else {
                   // let alert = UIAlertController(title: "Success", message:"You are Signed up!", preferredStyle: .Alert)
                    //alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    //self.presentViewController(alert, animated: true){}
                    self.performSegueWithIdentifier("toHomePage", sender: self)

                }
            })
        }
    }
    
    // MARK: Text Field Delegate Functions
    
    @IBAction func dismissKeyboard(recognizer: UITapGestureRecognizer) {
        recognizer.view?.endEditing(true)
    }
}

