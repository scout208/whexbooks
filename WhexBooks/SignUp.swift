//
//  SignUp.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/11/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

class SignUp: NSObject {
    
    
    var username: String
    var password: String
    var email: String
    
    init(uname: String, pword: String, mail: String)
    {
        self.username = uname
        self.password = pword
        self.email = mail
    }
    
    func hasEmptyFields() -> Bool {
        if username.isEmpty || password.isEmpty || email.isEmpty {
            return true
        }
        return false
    }
    
    func isValidEmail() -> Bool {
        let emailEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]"
        let range = email.rangeOfString(emailEx, options:.RegularExpressionSearch)
        let result = range != nil ? true: false
        return result
        
    }
    
    func storeSuccessfulSignUp() -> Bool {
        //performSegueWithIdentifier("fromHomePage", sender: self);
        return true
    }
    
}


