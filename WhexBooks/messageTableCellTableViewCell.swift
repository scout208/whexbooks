//
//  messageTableCellTableViewCell.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 12/1/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class messageTableCellTableViewCell: UITableViewCell {

    @IBOutlet weak var incomingUsername: UILabel!
    @IBOutlet var messageIndicatorImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
