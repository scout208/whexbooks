//
//  HomeViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/8/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    let globals:GlobalData = GlobalData.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentUser = PFUser.currentUser()
        let user = currentUser!.username
        print(user)
        
        //disables but doesnt remove
        self.navigationItem.hidesBackButton = true;
        
        let query = PFQuery(className:"book")
        let curUser = PFUser.currentUser()
        
        query.whereKey("neededUser", equalTo: curUser!)
        query.findObjectsInBackgroundWithBlock{
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        //print(object.objectId)
                        
                        var book = Book()
                        //book.image = object.valueForKey("bookImage") as! PFFile
                        book.authors = object.valueForKey("bookAuthor") as! String
                        book.isbn_10 = object.valueForKey("bookISBN") as! String
                        book.title = object.valueForKey("bookName") as! String
                        book.person = object.valueForKey("neededUser") as! PFUser
                        
                        self.globals.addBookNeeded(book.title)
                       // print(self.globals.booksNeeded)
                        //book.price = object.valueForKey("bookPrice") as! String
                        
                        
                        /*let image = object.valueForKey("bookImage") as! PFFile
                        
                        image.getDataInBackgroundWithBlock{
                        (imageData: NSData?, error:NSError?) -> Void in
                        if error == nil {
                        if let finalimage = UIImage(data:imageData!){
                        book.image? = finalimage
                        }
                        }*/
                        // }
                        dispatch_async(dispatch_get_main_queue()) {
                            //self.booksThatINeed.append(book)
                            //self.TableView.reloadData()
                        }
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
            
            print(self.globals.getLengthOfBookArr())
        }
        //print(currentUser!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToMessages(sender: AnyObject) {
        performSegueWithIdentifier("messagesSegue", sender: self)
    }

    @IBAction func booksThatiHave(sender: AnyObject) {
        
        performSegueWithIdentifier("books", sender: self)
    }
    
    @IBAction func logUserOut(sender: AnyObject) {
        
        PFUser.logOut();
        performSegueWithIdentifier("logoutSegue", sender: self)
    }
    
    @IBAction func goToMyProfile(sender: AnyObject) {
        
        performSegueWithIdentifier("goToMyProfile", sender: self)
        
    }
    @IBAction func goToMatches(sender: AnyObject) {
        performSegueWithIdentifier("matchesSegue", sender: self)
        
    }
    @IBAction func goToSellBooks(sender: AnyObject) {
        performSegueWithIdentifier("goToSell", sender: self)
    }

}
