//
//  BookTableViewCell.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

protocol MessageCellDelegate {
    func cellTapped(cell: BookTableViewCell)
}

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var subjectNameandEdition: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var ISBNLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var messageDelegate: MessageCellDelegate?
    
    @IBOutlet weak var messageButton: UIButton!
    
    
    @IBAction func messageButonTapped(sender: UIButton) {
        if let delegate = messageDelegate {
            delegate.cellTapped(self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadItem(book: Book) {
        subjectNameandEdition.text = book.title
        authorLabel.text = book.authors
        imageView!.image = book.image
        usernameLabel.text = book.userUsername
        ISBNLabel.text = book.isbn_10
        priceLabel.text = String(book.price)
    }
}
