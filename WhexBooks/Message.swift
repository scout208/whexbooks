//
//  Message.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 12/3/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import Foundation
import UIKit

struct Message{

    var message: String!
    var fromUserLabel: String!
    var toUserLabel: String!
    var fromUser = PFUser()
    var toUser = PFUser()
    var bookName: String
    var messageIncoming: Bool
    var date: String
    var cellString: String
    
    init () {
        message = ""
        bookName = ""
        messageIncoming = false
        date = ""
        cellString = ""
        fromUserLabel = ""
        toUserLabel = ""
        
    }
    mutating func setToUser(name: PFUser){
        toUser = name
    }
    mutating func setFromUser(name: PFUser){
        fromUser = name
        
    }
    
    func getToUserText() -> String{
        
        print("toUser")
        print(toUser)
        print("toUser")
        return String(toUser)
    }
    func getFromUserText() -> String{
        
        print("fromUser")
        print(fromUser)
        print("fromUser")

        return String(fromUser)
    }
}
    