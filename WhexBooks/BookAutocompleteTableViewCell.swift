//
//  BookAutocompleteTableViewCell.swift
//  WhexBooks
//
//  Created by uics14 on 11/27/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

protocol ButtonCellDelegate {
    func cellTapped(cell: BookAutocompleteTableViewCell)
}

class BookAutocompleteTableViewCell: UITableViewCell {

    @IBOutlet weak var bookTitleEdition: UITextField!
    @IBOutlet weak var bookAuthor: UITextField!
    @IBOutlet weak var bookISBN: UITextField!
    @IBOutlet weak var bookISBN13: UITextField!
    
    @IBOutlet weak var bookImage: UIImageView!
    
    @IBOutlet weak var addButton: UIButton!
    
    var buttonDelegate: ButtonCellDelegate?
    
    @IBAction func addButtonTapped(sender: UIButton) {
        if let delegate = buttonDelegate {
            delegate.cellTapped(self)
        }
        print("Called addButtonTapped")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("Called awakeFromNib")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadItem(book: Book) {
        bookTitleEdition.text = book.title
        bookAuthor.text = book.authors
        bookImage.image = book.image
        bookISBN.text = book.isbn_10
        bookISBN13.text = book.isbn_13
    }
}
