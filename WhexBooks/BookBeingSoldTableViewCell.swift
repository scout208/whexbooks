//
//  BookBeingSoldTableViewCell.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/10/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class BookBeingSoldTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ISBNLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var bookImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadItem(book: Book) {
        priceLabel.text = "$ \(book.price)"
        titleLabel.text = book.title
        authorLabel.text = book.authors
        bookImage.image = book.image
        ISBNLabel.text = book.isbn_10
        usernameLabel.text = book.userUsername
        
    }
    
    @IBAction func addBookToList(sender: AnyObject) {
        
        
    }

}
