//
//  loginViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class loginViewController: UIViewController, UITextFieldDelegate, UIAlertViewDelegate {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func resetPassword(sender: AnyObject) {
        
        performSegueWithIdentifier("resetPasswordSegue", sender: self)
    }
    
    @IBAction func loginAction(sender: AnyObject) {
        
        let username = self.usernameTextField.text
        let password = self.passwordTextField.text
        
        if username?.characters.count < 5 {
            let alert = UIAlertController(title: "Invalid", message:"Username must be greater than 5 characters", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
            
        } else if password?.characters.count < 8 {
            let alert = UIAlertController(title: "Invalid", message:"Password must be greater than 8 characters", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
            
        } else {
            // Run a spinner to show a task in progress
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            // Send a request to login
            PFUser.logInWithUsernameInBackground(username!, password: password!, block: { (user, error) -> Void in
                
                // Stop the spinner
                spinner.stopAnimating()
                
                if ((user) != nil) {

                    self.performSegueWithIdentifier("loginToHomeScreen", sender: self)

                    
                } else {
                    let alert = UIAlertController(title: "Error", message:"Error", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}

                }
            })
        }
        
    }

}
