//
//  booksThatINeedViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class booksThatINeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ButtonCellDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var searchField: UISearchBar!
    
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var autocompleteTableView: UITableView!
    
    var autocompleteBooks = [Book]()
    var booksThatINeed = [Book]()
    
    private let booksAPIKey = "AIzaSyCaeWV0hkGyxfXVEMHdijhaHOIIA0im1UQ"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        let nib = UINib(nibName: "BookBeingSoldCell", bundle: nil)
//        TableView.registerNib(nib, forCellReuseIdentifier: "BookBeingSoldCell")
        // Do any additional setup after loading the view.
        //let nib = UINib(nibName: "BookBeingSoldCell", bundle: nil)
        //TableView.registerNib(nib, forCellReuseIdentifier: "BookBeingSoldCell")
        
        let anotherNib = UINib(nibName: "BookAutocompleteTableViewCell", bundle: nil)
        autocompleteTableView.registerNib(anotherNib, forCellReuseIdentifier: "BookAutocompleteTableViewCell")
        
        TableView.registerNib(anotherNib, forCellReuseIdentifier: "BookThatINeedCell")
        
        let query = PFQuery(className:"book")
        let curUser = PFUser.currentUser()
        
        query.whereKey("neededUser", equalTo: curUser!)
        query.findObjectsInBackgroundWithBlock{
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        //print(object.objectId)
                        
                        var book = Book()
                        //book.image = object.valueForKey("bookImage") as! PFFile
                        book.authors = object.valueForKey("bookAuthor") as! String
                        book.isbn_10 = object.valueForKey("bookISBN") as! String
                        book.title = object.valueForKey("bookName") as! String
                        book.person = object.valueForKey("neededUser") as! PFUser
                        //book.price = object.valueForKey("bookPrice") as! String
                        
                        
                        let image = object.valueForKey("bookImage") as! PFFile
                        
                        image.getDataInBackgroundWithBlock{
                            (imageData: NSData?, error:NSError?) -> Void in
                            if error == nil {
                                if let finalimage = UIImage(data:imageData!){
                                    book.image = finalimage
                                    self.booksThatINeed.append(book)
                                    self.TableView.reloadData()
                                }
                            }
                        
                        }
                        dispatch_async(dispatch_get_main_queue()) {

                        }
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
        
        
        
        //TableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 10
        
        if tableView == autocompleteTableView {
            count = autocompleteBooks.count
        }
        else if tableView == TableView {
            count = booksThatINeed.count
        }
        
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cellIdentifier: String!
        
        var book:Book!
        
        if tableView == TableView {
            
            cellIdentifier = "BookThatINeedCell"
            
            book = booksThatINeed[indexPath.row]
            
        }
        else if tableView == autocompleteTableView {
            
            cellIdentifier = "BookAutocompleteTableViewCell"
            
            book = autocompleteBooks[indexPath.row]
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! BookAutocompleteTableViewCell
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.grayColor().CGColor
        
        cell.loadItem(book)
        
        if tableView == autocompleteTableView {
            cell.addButton.hidden = false
            
            // For the + button in the autocomplete cells
            if cell.buttonDelegate == nil {
                cell.buttonDelegate = self
            }
        } else {
            cell.addButton.hidden = true
        }
        
        return cell
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    
    // MARK: - ButtonCellDelegate
    
    func cellTapped(cell: BookAutocompleteTableViewCell) {
        autocompleteTableView.hidden = true
        self.addBook(autocompleteBooks[autocompleteTableView.indexPathForCell(cell)!.row])
        autocompleteBooks.removeAll()
    }
    
    func addBook(book: Book) {
        let alert = UIAlertController(
            title: book.title,
            message: "Add button for book, \(book.title) was tapped!",
            preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Gotcha!", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(
            alert,
            animated: true,
            completion: nil)
        
        
        
        let query = PFQuery(className: "book")
        let newBook = PFObject(className: "book")
        newBook["bookName"] = book.title
        newBook["bookAuthor"] = book.authors
        newBook["bookISBN"] = book.isbn_10
        newBook["neededUser"] = PFUser.currentUser()!
        newBook["neededUserUsername"] = PFUser.currentUser()!.username!
        print(PFUser.currentUser()!.username!)
        
        let imageData = UIImagePNGRepresentation(book.image)
        let imageFile:PFFile = PFFile(data: imageData!)!
        newBook.setObject(imageFile, forKey: "bookImage")
        
        
        newBook.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                print("success")
                
                query.getFirstObjectInBackgroundWithBlock{(object:PFObject?, error: NSError?) -> Void in
                    if error == nil {
                        
                        PFUser.currentUser()!.saveInBackground()
                    }
                    else
                    {
                        print("error")
                    }
                }
            }
            else {
                print("not successful")
            }
        }
        newBook.saveInBackground()
        
        booksThatINeed.append(book)
        TableView.reloadData()
    }
    
    // MARK: - Search Bar Delegate
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        let substring = (searchField.text! as NSString).stringByReplacingCharactersInRange(range, withString: text)
        
        searchBooksWithSubstring(substring)
        return true
    }
    
    func searchBooksWithSubstring(substring: String) {
        autocompleteBooks.removeAll(keepCapacity: false)
        
        let booksService = BooksService(APIKey: booksAPIKey)
        booksService.getBooks(substring) {
            (let bookResults) in
            self.autocompleteBooks = bookResults!
            dispatch_async(dispatch_get_main_queue()) {
                self.autocompleteTableView.reloadData()
            }
        }
        
        autocompleteTableView.hidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
