//
//  GlobalData.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 12/10/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import Foundation

class GlobalData{
    
    static let sharedInstance = GlobalData()
    var booksNeeded = [String]()
    
    func addBookNeeded(addBookToArr: String){
        
        self.booksNeeded.append(addBookToArr)
    }
    
    func getLengthOfBookArr() -> Int{
        return booksNeeded.count
    }
}