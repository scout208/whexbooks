//
//  step4ViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class step4ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var pictureLabel: UILabel!
    
    var bookBeingAdded: Book!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        nameLabel.text = bookBeingAdded.title
        priceLabel.text = "$ \(bookBeingAdded.price)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func submitNewBook(sender: AnyObject) {
        
        let query = PFQuery(className: "book")
        
        if nameLabel.text == "" || priceLabel == ""
        {
            let alert = UIAlertController(title: "Invalid", message:" Empty Fields Exist", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
        else
        {
            
            let book = PFObject(className: "book")
            book["bookName"] = bookBeingAdded.title
            book["bookPrice"] = bookBeingAdded.price
            book["CreatedBy"] = PFUser.currentUser()!
            book["CreatedByUsername"] = PFUser.currentUser()?.username
            bookBeingAdded.userUsername = PFUser.currentUser()?.username
            print(PFUser.currentUser()?.username)
            book["bookISBN"] = bookBeingAdded.isbn_10
            book["bookAuthor"] = bookBeingAdded.authors
            let imageData = UIImagePNGRepresentation(bookBeingAdded.image)
            let imageFile:PFFile = PFFile(data: imageData!)!
            book.setObject(imageFile, forKey: "bookImage")
            
            
            book.saveInBackgroundWithBlock {
                (success: Bool, error: NSError?) -> Void in
                if (success) {
                    print("success")
                    
                    query.getFirstObjectInBackgroundWithBlock{(object:PFObject?, error: NSError?) -> Void in
                        if error == nil {
                            
                            PFUser.currentUser()!.saveInBackground()
                        }
                        else
                        {
                            print("error")
                        }
                    }
                }
                else {
                    print("not successful")
                }
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
      
    }

}
