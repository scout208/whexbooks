//
//  messageCellClass.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 12/3/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class messageCellClass: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var incomingMail: UIButton!
    @IBOutlet weak var outgoingMail: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func loadItem(message: Message) {
        //messageLabel.text = message.fromUser
        
        //messageLabel.text = message.cellString
        bookNameLabel.text = message.bookName
        date.text = message.date
        messageLabel.text = message.fromUserLabel
        
        if(message.messageIncoming == true)
        {
            outgoingMail.hidden = true
            incomingMail.hidden = false
        }
        else
        {
            outgoingMail.hidden = false
            incomingMail.hidden = true
        }
        
    }
}