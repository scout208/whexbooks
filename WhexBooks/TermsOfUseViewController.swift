//
//  TermsOfUseViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/12/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class TermsOfUseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okToReturnToSignUp(sender: AnyObject) {
        
        performSegueWithIdentifier("backToSignUp", sender: self)
    }

}
