//
//  BooksThatIHaveViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/10/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class BooksThatIHaveViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var booksThatIHave = [Book]()
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "BookBeingSoldCell", bundle: nil)
        
        tableView.registerNib(nib, forCellReuseIdentifier: "BookBeingSoldCell")
        
        
        let query = PFQuery(className:"book")
        let curUser = PFUser.currentUser()
        
        query.whereKey("CreatedBy", equalTo: curUser!)
        query.findObjectsInBackgroundWithBlock{
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        
                        print("got new book")
                        var book = Book()
                        //book.image = object.valueForKey("bookImage") as! UIImage
                        book.authors = object.valueForKey("bookAuthor") as! String
                        book.isbn_10 = object.valueForKey("bookISBN") as! String
                        book.title = object.valueForKey("bookName") as! String
                        book.price = object.valueForKey("bookPrice") as! String
                        book.person = object.valueForKey("CreatedBy") as! PFUser!
                        book.userUsername = object.valueForKey("CreatedByUsername") as! String
                        
                        let image = object.objectForKey("bookImage") as! PFFile
                        
                        image.getDataInBackgroundWithBlock{
                            (imageData: NSData?, error:NSError?) -> Void in
                            if error == nil {
                                print("here")

                                if let finalimage = UIImage(data: imageData!){
                                    book.image = finalimage
                                    self.booksThatIHave.append(book)
                                    self.tableView.reloadData()
                                }
                                //finalimage = UIImage(data:imageData!)!
                                    //book.image? = finalimage
                                
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            //self.booksThatIHave.append(book)
                            //self.tableView.reloadData()
                        }
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return booksThatIHave.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("BookBeingSoldCell") as! BookBeingSoldTableViewCell
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.grayColor().CGColor
        
        let book = booksThatIHave[indexPath.row]
        
        cell.loadItem(book)
        
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            booksThatIHave.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 71
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.tableView.reloadData()
    }
    
    
    // MARK: NAVIGATION
    
    @IBAction func saveBook(segue: UIStoryboardSegue) {
        if let step4ViewController = segue.sourceViewController as? step4ViewController {
            if let newBook = step4ViewController.bookBeingAdded {
                booksThatIHave.append(newBook)
                tableView.reloadData()
            }
        }
    }

}
