//
//  myProfileViewController.swift
//  WhexBooks
//
//  Created by Nathan Pierotti on 11/9/15.
//  Copyright © 2015 Nathan Pierotti. All rights reserved.
//

import UIKit

class myProfileViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            let user = currentUser!.username
            usernameLabel.text = user
        
            
            let query = PFQuery(className:"_User")
            let curUser = PFUser.currentUser()?.username
            
            print(curUser)
            query.whereKey("username", equalTo: curUser!)
            query.findObjectsInBackgroundWithBlock{
                (objects: [PFObject]?, error: NSError?) -> Void in
                
                print("here")
                
                if error == nil {
                    print("Successfully retrieved \(objects!.count) scores.")
                    if let objects = objects {
                        for object in objects {
                            print(object.objectId)
                            
                            var rating = object.valueForKey("rating") as? String
                            if rating == nil{
                                rating = "5"
                            }else{
                            
                                self.ratingLabel.text = rating
                            }
                        }
                    }
                } else {
                    print("Error: \(error!) \(error!.userInfo)")
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {

    }
    

 

}
