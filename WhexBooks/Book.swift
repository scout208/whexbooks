//
//  Book.swift
//  Google Books Tester
//
//  Created by uics14 on 11/16/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import Foundation
import UIKit

struct Book {
    var title: String!
    var authors: String!
    var image: UIImage!
    var price: String!
    var isbn_10: String!
    var isbn_13: String!
    var person: PFUser!
    var personThatCreatedBook: PFUser!
    var userUsername: String!
    //let publisher: String
    //let publishedDate: NSDate
    //let description: String
    
    init () {
        title = ""
        authors = ""
        image = nil
        price = ""
        isbn_10 = "unknown"
        isbn_13 = "unknown"
        userUsername = ""
    }
    
    init (bookDictionary: JSON) {
        title = bookDictionary["title"].stringValue
        authors = bookDictionary["authors"].arrayValue.map { "\($0)" }.joinWithSeparator(", ")
        
        if let industryIdentifiers = bookDictionary["industryIdentifiers"].array {
            for item in industryIdentifiers {
                switch item["type"].stringValue {
                    case "ISBN_13":
                        isbn_13 = item["identifier"].stringValue
                        break;
                    case "ISBN_10":
                        isbn_10 = item["identifier"].stringValue
                        break;
                    default:
                        isbn_13 = "ISBN_13 not found"
                        isbn_10 = "ISBN_10 not found"
                        break;
                }
            }
        }
        else {
            print("unable to get ISBN")
        }
        
        let imageURL = NSURL(string: bookDictionary["imageLinks"]["smallThumbnail"].stringValue)
        do {
            let imageData = try NSData(contentsOfURL: imageURL!, options: NSDataReadingOptions())
            image = UIImage(data: imageData)!
        } catch {
            print(error)
        }
        
    }
}